package com.example.viikko11p4;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class fragment_fonttikoko extends Fragment {
    EditText teksti;
    String fontti;
    View view;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        return inflater.inflate(R.layout.fonttikoko_layout, container, false);
    }

    public String onViewCreated(Bundle saveInstaceState) {
        teksti = (EditText) view.findViewById(R.id.editText);
        button = (Button) view.findViewById(R.id.button);

        return fontti;
    }

}
